<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Insly Calculator</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <link rel="stylesheet" href="/css/files/bootstrap.min.css">

</head>
<body>


<div class="container">

   <div class="row mt-5">
       <div class="col-md-12 col-sm-12">
           <div class="panel panel-info">
               <div class="panel-heading"> Instalment Calculator </div>
               <div class="panel-body">
                   <div class="table-responsive">
                       <form action="{{route('calculator')}}" method="post">
                           {{csrf_field()}}
                           <div class="form-group">
                               <label for="car_value">Car Value :</label>
                               <input name="car_value" type="text" class="form-control" id="car_value">
                           </div>
                           <div class="form-group">
                               <label for="tax">Tax :</label>
                               <input name="tax" type="text" class="form-control" id="tax">
                           </div>
                           <div class="form-group">
                               <label for="instalment">Select Instalment :</label>
                               <select name="instalment" class="form-control" id="instalment">
                                   <option value="1"> 1 </option>
                                   <option value="2"> 2 </option>
                                   <option value="3"> 3 </option>
                                   <option value="4"> 4 </option>
                                   <option value="5"> 5 </option>
                                   <option value="6"> 6 </option>
                                   <option value="7"> 7 </option>
                                   <option value="8"> 8 </option>
                                   <option value="9"> 9 </option>
                                   <option value="10"> 10 </option>
                                   <option value="11"> 11 </option>
                                   <option value="12"> 12 </option>
                               </select>
                           </div>

                           <input type="submit" class="btn btn-success btn-sm pull-right" value="Calculate">
                       </form>
                   </div>
               </div>
           </div>
           @if(count($errors) > 0)

               <div class="alert alert-danger">

                   <ul>
                       @foreach($errors->all() as $error)
                           <li>{{$error}}</li>
                       @endforeach
                   </ul>
               </div>

           @endif
       </div>
   </div>

</div>


<script src="/js/files/bootstrap.min.js"></script>

</body>
</html>
