<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Insly Calculator</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <link rel="stylesheet" href="/css/files/bootstrap.min.css">

</head>
<body>


<div class="container">

    <div class="row mt-5">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-warning">
                <div class="panel-heading"> Instalments</div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th></th>
                            <th>policy</th>
                            @foreach($totalInstalments as $instalment)
                                <th>instalment {{$loop->index + 1}} </th>
                            @endforeach
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td>value</td>
                            <td>{{$carValue}}</td>
                            @foreach($totalInstalments as $instalment)
                                <td> </td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Base Premium</td>
                            <td>{{$basePremium}}</td>
                            @foreach($totalInstalments as $instalment)
                                <td>{{$instalment['base_premium']}} </td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Commission</td>
                            <td>{{$baseCommission}}</td>
                            @foreach($totalInstalments as $instalment)
                                <td>{{$instalment['commission']}} </td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Tax</td>
                            <td>{{$baseTax}}</td>
                            @foreach($totalInstalments as $instalment)
                                <td>{{$instalment['tax']}} </td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Total Cost</td>
                            <td>{{$totalCost}}</td>
                            @foreach($totalInstalments as $instalment)
                                <td>{{$instalment['total_cost']}} </td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            @if(count($errors) > 0)

                <div class="alert alert-danger">

                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>

            @endif
        </div>

        <dev style="margin:auto ; text-align: center ; display: block">
            <a href="{{route('home')}}"> <button class="btn btn-primary" > Calculate Another </button></a>
        </dev>
    </div>



</div>


<script src="/js/files/bootstrap.min.js"></script>

</body>
</html>
