<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Employee extends Authenticatable
{

    protected $fillable = [
        'name' ,
        'email' ,
        'password' ,
        'phone' ,
        'birthday' ,
        'address' ,
        'id_number' ,
        'is_current_employee' ,
        'en_introduction' ,
        'sp_introduction' ,
        'fr_introduction' ,
        'en_prev_work' ,
        'sp_prev_work' ,
        'fr_prev_work' ,
        'en_education_info' ,
        'sp_education_info' ,
        'fr_education_info' ,
        'created_by' ,
        'updated_by' ,
    ];

}


