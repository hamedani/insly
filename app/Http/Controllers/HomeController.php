<?php

namespace App\Http\Controllers;

use App\Http\Requests\CalculatorRequest;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class HomeController extends BaseController
{
    use AuthorizesRequests , DispatchesJobs , ValidatesRequests;


    public function index ()
    {

        return view( 'home' );

    }

    public function myName ()
    {
        $binary = '01101101 01101111 01101000 01100001 01101101 01101101 01100001 01100100 01110010 01100101 01111010 01100001 00100000 01101000 01100001 01101101 01100101 01100100 01100001 01101110 01101001';

        $binaries = explode( ' ' , $binary );

        $string = NULL;
        foreach ( $binaries as $binary ) {
            $string .= pack( 'H*' , dechex( bindec( $binary ) ) );
        }

        return $string;

    }
}

