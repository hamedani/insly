<?php

namespace App\Http\Controllers;

use App\Services\CalculatorService;
use App\Http\Requests\CalculatorRequest;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class CalculatorController extends BaseController
{
    use AuthorizesRequests , DispatchesJobs , ValidatesRequests;

    public $calculatorService;

    public function __construct ( CalculatorService $calculatorService )
    {
        $this->calculatorService = $calculatorService;
    }

    public function calculate ( CalculatorRequest $request )
    {

        $carValue   = $request->input( 'car_value' );
        $tax        = $request->input( 'tax' );
        $instalment = $request->input( 'instalment' );

        $data = $this->calculatorService->calculator( $carValue , $tax , $instalment );

        $basePremium      = $data[ 'base_premium' ];
        $baseTax          = $data[ 'base_tax' ];
        $baseCommission   = $data[ 'base_premium' ];
        $totalCost        = $data[ 'total_cost' ];
        $totalInstalments = $data[ 'total_instalments' ];


        return view( 'instalments' , compact( 'basePremium' , 'baseTax' , 'baseCommission' , 'totalCost' , 'totalInstalments' , 'carValue' ) );


    }
}

