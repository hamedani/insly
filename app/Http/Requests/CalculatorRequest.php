<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CalculatorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules ()
    {
        if ( $this->method() == 'POST' ) {
            return [
                'car_value'  => 'required|numeric|min:100|max:100000' ,
                'tax'        => 'required|numeric|min:0|max:100' ,
                'instalment' => 'required|numeric|min:1|max:12' ,
            ];
        }
    }
}
