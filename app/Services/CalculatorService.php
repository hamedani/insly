<?php


namespace App\Services;


use Carbon\Carbon;
use Brick\Math\BigDecimal;
use Brick\Math\RoundingMode;

class CalculatorService extends AbstractService implements ServiceInterface
{


    public $basePremium , $baseTax , $baseCommission , $totalCost , $totalInstalments = [];

    public function calculator ( $carValue , $tax , $instalment )
    {


        $this->basePremium    = $this->calculateBasePremium( $carValue );
        $this->baseTax        = $this->calculateTax( $tax );
        $this->baseCommission = $this->calculateCommission();
        $this->totalCost      = $this->calculateTotalCost();
        $this->calculateInstalments( $instalment );

        return [ 'base_premium'      => $this->basePremium ,
                 'base_tax'          => $this->baseTax ,
                 'base_commission'   => $this->baseCommission ,
                 'total_cost'        => $this->totalCost ,
                 'total_instalments' => $this->totalInstalments,
        ];

    }

    private function calculateBasePremium ( $carValue )
    {

        $now   = new Carbon();
        $start = Carbon::createFromTime( 15 , 0 );
        $end   = Carbon::createFromTime( 20 , 0 );


        if ( $now->dayOfWeek == 5 && $now->between( $start , $end ) ) {

            return BigDecimal::of( $carValue )->multipliedBy( 13 )->dividedBy( 100 );

        }

        return BigDecimal::of( $carValue )->multipliedBy( 11 )->dividedBy( 100 );

    }

    private function calculateCommission ()
    {
        return BigDecimal::of( $this->basePremium )->multipliedBy( 17 )->dividedBy( 100 );
    }

    private function calculateTax ( $tax )
    {
        return BigDecimal::of( $this->basePremium )->multipliedBy( $tax )->dividedBy( 100 );
    }

    private function calculateTotalCost ()
    {

        return BigDecimal::of( $this->basePremium )->plus( $this->baseTax )->plus( $this->baseCommission );
    }

    private function calculateInstalments ( $instalment )
    {

        for ( $c = 1 ; $c <= $instalment ; $c ++ ) {

            $this->totalInstalments[ $c ][ 'base_premium' ] = BigDecimal::of( $this->basePremium )->dividedBy( $instalment , 3 , RoundingMode::DOWN );
            $this->totalInstalments[ $c ][ 'tax' ]          = BigDecimal::of( $this->baseTax )->dividedBy( $instalment , 3 , RoundingMode::DOWN );
            $this->totalInstalments[ $c ][ 'commission' ]   = BigDecimal::of( $this->baseCommission )->dividedBy( $instalment , 3 , RoundingMode::DOWN );
            $this->totalInstalments[ $c ][ 'total_cost' ]   = BigDecimal::of( $this->totalCost )->dividedBy( $instalment , 3 , RoundingMode::DOWN );

        }

        return $this->totalInstalments;

    }


}
