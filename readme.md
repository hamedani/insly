
## Tasks One Route


- Route::get('/name' , 'HomeController@myName')->name('name');




## Task Two Route

- Route::get('/' , 'HomeController@index')->name('home');
- Route::post('/calculator' , 'CalculatorController@calculate')->name('calculator');



## Task three 

- this task includes migration and sql file in this project (migrate.sql) and the (create_employee_table.php)
- also a unit test which the name is (EmployeeTest.php)
