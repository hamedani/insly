<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->date('birthday');
            $table->text('address');
            $table->timestamp('id_number')->nullable();
            $table->boolean('is_current_employee')->default(1);
            $table->text('en_introduction');
            $table->text('sp_introduction');
            $table->text('fr_introduction');
            $table->text('en_prev_work');
            $table->text('sp_prev_work');
            $table->text('fr_prev_work');
            $table->text('en_education_info');
            $table->text('sp_education_info');
            $table->text('fr_education_info');
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamps();

            // select * from employee where id = 1 ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
