<?php

namespace Tests\Unit\Service;

use App\Employee;
use Tests\TestCase;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EmployeeTest extends TestCase
{
    use DatabaseTransactions;

    protected $faker;

    /**
     * Restrict Fields for update
     */


    public function setUp () : void
    {
        parent::setUp();

        $this->faker = $this->app->make( Faker::class );
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreate ()
    {
        $name              = $this->faker->name;
        $text              = $this->faker->realText;
        $fakeData      = [
            'name'                => $this->faker->name ,
            'email'               => $this->faker->email ,
            'password'            => $this->faker->password ,
            'phone'               => $this->faker->mobileNumber ,
            'birthday'            => $this->faker->name ,
            'address'             => $this->faker->address ,
            'id_number'           => $this->faker->ssn ,
            'en_introduction'     => $this->faker->text ,
            'sp_introduction'     => $this->faker->text ,
            'fr_introduction'     => $this->faker->text ,
            'en_prev_work'        => $this->faker->text ,
            'sp_prev_work'        => $this->faker->text ,
            'fr_prev_work'        => $this->faker->text ,
            'en_education_info'   => $this->faker->text ,
            'sp_education_info'   => $this->faker->text ,
            'fr_education_info'   => $this->faker->text ,
            'created_by'          => $this->faker->name ,
            'updated_by'          => $this->faker->name ,
        ];
        $persistedData = Employee::create (  $fakeData  );
        $this->assertInstanceOf( Employee::class , $persistedData );
    }

}
